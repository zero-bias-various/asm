//@flow

import {compile, exec} from './index'
import {Method, Variable} from './strongReference'

describe('Variable', () => {
  it('allow basic crud', () => {
    const fn = jest.fn()
    const foo = Variable('foo')
    const log = Method('log')
    exec(
      compile([
        foo.define(),
        log.define((...args) => fn(...args)),
        foo.set(0),
        log.call(foo),
        foo.set(1),
        log.call(foo),
      ]),
    )
    expect(fn.mock.calls).toEqual([[0], [1]])
  })

  it('throw at compile time when argument is used before definition', () => {
    const fn = jest.fn()
    const foo = Variable('foo')
    const log = Method('log')
    expect(() =>
      //prettier-ignore
      compile([
        log.define((...args) => fn(...args)),
        log.call(foo),
        foo.define(),
        foo.set(0),
      ]),
    ).toThrowError('variable "foo" is not defined')
    expect(fn).not.toHaveBeenCalled()
  })
  it('throw at compile time when argument is not defined', () => {
    const fn = jest.fn()
    const log = Method('log')
    const foo = Variable('foo')
    expect(() =>
      //prettier-ignore
      compile([
        log.define((...args) => fn(...args)),
        log.call(foo),
      ]),
    ).toThrowError('variable "foo" is not defined')
    expect(fn).not.toHaveBeenCalled()
  })
  it('throw at compile time on variable redeclaration', () => {
    const foo = Variable('foo')
    expect(() =>
      //prettier-ignore
      compile([
        foo.define(),
        foo.set(0),
        foo.define(),
      ]),
    ).toThrowError('variable "foo" is already defined')
  })
})

describe('Method', () => {
  it('call setted function with argument', () => {
    const fn = jest.fn()
    const log = Method('log')
    const foo = Variable('foo')
    exec(
      compile([
        foo.define(),
        foo.set(1),
        log.define((...args) => fn(...args)),
        log.call(foo),
        foo.set(2),
        log.call(foo),
      ]),
    )
    expect(fn.mock.calls).toEqual([[1], [2]])
  })
  it('call setted function without arguments', () => {
    const fn = jest.fn()
    const log = Method('log')
    exec(
      //prettier-ignore
      compile([
        log.define((...args) => fn(...args)),
        log.call(),
        log.call(),
      ]),
    )
    expect(fn.mock.calls).toEqual([[], []])
  })
  it('call setted function with many arguments', () => {
    const fn = jest.fn()
    const log1 = Method('log1')
    const foo = Variable('foo')
    const bar = Variable('bar')
    exec(
      compile([
        foo.define(),
        bar.define(),
        foo.set(1),
        bar.set(2),
        log1.define((...args) => fn(...args)),
        log1.call(foo, bar),
        foo.set(-1),
        bar.set(-2),
        log1.call(foo, bar),
      ]),
    )
    expect(fn.mock.calls).toEqual([[1, 2], [-1, -2]])
  })
  it('doesnt fail on throw', () => {
    const log = Method('log')
    const code = compile([
      log.define(() => {
        throw new Error('[should fail]')
      }),
      log.call(),
    ])
    expect(() => exec(code)).not.toThrow()
  })
  it('call example', () => {
    const foo = Variable('foo')
    const photo = Variable('photo')
    const log = Method('log')
    const code = compile([
      foo.define(),
      foo.set(1),
      photo.define(),
      photo.set({photo: 'a'}),
      log.define(e => {
        console.log('latest: ', e)
      }),
      photo.set({photo: 'z'}),
      log.call(photo),
      photo.set({photo: 'b'}),
      log.call(photo),
      photo.unset(),
      log.call(photo),
    ])
    expect(() => exec(code)).not.toThrow()
  })
})
