//@flow

import {scopeDef} from './stdlib/typedef'

export const fun = scopeDef('fun', {
  define: {
    name: 'define',
    create: ['name', 'fn'],
  },
  call: {
    name: 'call',
    create: (name, ...args) => ({name, args}),
  },
})

export const val = scopeDef('val', {
  define: {
    name: 'define',
    create: ['name'],
  },
  set: {
    name: 'push',
    create: ['name', 'value'],
  },
  unset: {
    name: 'pop',
    create: ['name'],
  },
  read: {
    name: 'read',
    create: ['name', 'fn'],
  },
})

export const parse = scopeDef('parse', {
  walkBack: {
    name: 'walkBack',
    create: ({staticView, realtimeView}) => ({
      staticView,
      realtimeView,
    }),
  },
  readCallstack: {
    name: 'readCallstack',
    create: ['staticView'],
  },
})

export const tag = scopeDef('tag', {
  push: {
    name: 'push',
    create: ['name'],
  },
  pop: {
    name: 'pop',
    create: ['name'],
  },
})
