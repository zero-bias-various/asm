//@flow

import {compile, exec} from './index'
import {val} from './coreApi'
import {Method, Variable} from './strongReference'
import {decallstack, destackdata} from './stdlib/destack'

it('works', () => {
  const foo = Variable('foo')
  const photo = Variable('photo')
  const log = Method('log')
  const program = compile([
    foo.define(),
    photo.define(),
    log.define(e => {
      console.log('latest: ', e)
    }),
    foo.set(0),
    foo.set(1),
    val.read('foo', foo => {
      console.log('var foo', foo)
    }),
    photo.set({photo: 'a'}),
    log.call(photo),
    val.read('foo', foo => {
      console.log('var foo', foo)
    }),
    photo.set({photo: 'b'}),
    log.call(photo),
    photo.unset(),
    log.call(photo),
  ])
  expect(program).toBeDefined()
  exec(program)
})

describe('compile', () => {
  it('returns defined value', () => {
    const foo = Variable('foo')
    const photo = Variable('photo')
    const log = Method('log')
    const program = compile([
      foo.define(),
      photo.define(),
      log.define(e => {
        console.log('latest: ', e)
      }),
      photo.set({photo: 'a'}),
      log.call(photo),
      photo.set({photo: 'b'}),
      log.call(photo),
      photo.unset(),
      log.call(photo),
    ])
    expect(program).toBeDefined()
  })
})
it('readCallstack', () => {
  const fnBefore = jest.fn()
  const fnAfter = jest.fn()
  const log = Method('log')
  const foo = Variable('foo')
  const photo = Variable('photo')
  const logFn = (...args) => console.log(...args)
  const result = compile([
    foo.define(),
    foo.set(1),
    log.define(logFn),
    log.call(foo),
    photo.define(),
    photo.set({photo: 'a'}),
    foo.set(2),
    photo.set({photo: 'b'}),
    log.call(foo),
  ])

  expect(destackdata(result.step)).toEqual([
    ['tag/push', 'foo'],
    ['tag/push', 'foo'],
    ['tag/push', 'log'],
    ['tag/push', 'log'],
    ['fun/call', ['log', ['foo']]],
    ['tag/push', 'photo'],
    ['tag/push', 'photo'],
    ['tag/push', 'foo'],
    ['tag/push', 'photo'],
    ['fun/call', ['log', ['foo']]],
  ])

  //prettier-ignore
  expect(destackdata(result.local.stack.photo)).toEqual([
    ['val/push', ['photo', null]],
    ['tag/pop', 'photo'],
    ['val/push', {
      name: 'photo',
      value: {
        photo: 'a',
      },
    }],
    ['tag/pop', 'photo'],
    ['val/push', {
      name: 'photo',
      value: {
        photo: 'b',
      },
    }],
    ['tag/pop', 'photo'],
  ])

  //prettier-ignore
  expect(destackdata(result.local.stack.foo)).toEqual([
    ['val/push', ['foo', null]],
    ['tag/pop', 'foo'],
    ['val/push', {
      name: 'foo',
      value: 1,
    }],
    ['tag/pop', 'foo'],
    ['val/push', {
      name: 'foo',
      value: 2,
    }],
    ['tag/pop', 'foo'],
  ])

  expect(result).toEqual({
    local: {
      names: ['photo', 'log', 'foo'],
      stack: {
        foo: {
          cmd: 'val/push',
          data: ['foo', null],
          next: {
            cmd: 'tag/pop',
            data: 'foo',
            next: {
              cmd: 'val/push',
              data: {name: 'foo', value: 1},
              next: {
                cmd: 'tag/pop',
                data: 'foo',
                next: {
                  cmd: 'val/push',
                  data: {name: 'foo', value: 2},
                  next: {cmd: 'tag/pop', data: 'foo', next: undefined},
                },
              },
            },
          },
        },
        log: {
          cmd: 'val/push',
          data: ['log', null],
          next: {
            cmd: 'tag/pop',
            data: 'log',
            next: {
              cmd: 'val/push',
              data: {name: 'log', value: logFn},
              next: {cmd: 'tag/pop', data: 'log', next: undefined},
            },
          },
        },
        photo: {
          cmd: 'val/push',
          data: ['photo', null],
          next: {
            cmd: 'tag/pop',
            data: 'photo',
            next: {
              cmd: 'val/push',
              data: {name: 'photo', value: {photo: 'a'}},
              next: {
                cmd: 'tag/pop',
                data: 'photo',
                next: {
                  cmd: 'val/push',
                  data: {name: 'photo', value: {photo: 'b'}},
                  next: {cmd: 'tag/pop', data: 'photo', next: undefined},
                },
              },
            },
          },
        },
      },
    },
    step: {
      cmd: 'tag/push',
      data: 'foo',
      next: {
        cmd: 'tag/push',
        data: 'foo',
        next: {
          cmd: 'tag/push',
          data: 'log',
          next: {
            cmd: 'tag/push',
            data: 'log',
            next: {
              cmd: 'fun/call',
              data: ['log', ['foo']],
              next: {
                cmd: 'tag/push',
                data: 'photo',
                next: {
                  cmd: 'tag/push',
                  data: 'photo',
                  next: {
                    cmd: 'tag/push',
                    data: 'foo',
                    next: {
                      cmd: 'tag/push',
                      data: 'photo',
                      next: {
                        cmd: 'fun/call',
                        data: ['log', ['foo']],
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
    type: 'compiled',
  })
})
