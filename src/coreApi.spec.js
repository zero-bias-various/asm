//@flow

import {compile, exec} from './index'
import {parse} from './coreApi'
import {Method, Variable} from './strongReference'
import {decallstack} from './stdlib/destack'

function callTableView(callstack) {
  return decallstack(callstack).map(_ => _ && [_.procedure, _.args])
}

describe('parse def', () => {
  it('walkBack', () => {
    const fn = jest.fn()
    const fnRT = jest.fn()
    const log = Method('log')
    const foo = Variable('foo')
    const photo = Variable('photo')
    const logFn = (...args) => console.log(...args)
    exec(
      compile([
        foo.define(),
        foo.set(1),
        log.define(logFn),
        log.call(foo),
        photo.define(),
        photo.set({photo: 'a'}),
        foo.set(2),
        photo.set({photo: 'b'}),
        parse.walkBack({
          staticView(csdata, count) {
            fn(count, csdata.procedure, csdata.args)
          },
          realtimeView(procedure, data, variableName, count) {
            fnRT(count, procedure, variableName, data)
          },
        }),
        log.call(foo),
      ]),
    )
    expect(fn.mock.calls).toEqual([
      [0, 'noop', {}],
      [1, 'val/push', {name: 'photo', value: {photo: 'b'}}],
      [2, 'val/push', {name: 'foo', value: 2}],
      [3, 'val/push', {name: 'photo', value: {photo: 'a'}}],
      [4, 'val/define', {name: 'photo'}],
      [5, 'fun/call', {args: ['foo'], name: 'log'}],
      [6, 'val/push', {name: 'log', value: logFn}],
      [7, 'val/define', {name: 'log'}],
      [8, 'val/push', {name: 'foo', value: 1}],
      [9, 'val/define', {name: 'foo'}],
    ])
    expect(fnRT.mock.calls).toEqual([
      [9, 'val/define', 'foo', null],
      [8, 'val/push', 'foo', 1],
      [7, 'val/define', 'log', null],
      [6, 'val/push', 'log', logFn],
      [5, 'fun/call', 'log', logFn],
      [4, 'val/define', 'photo', null],
      [3, 'val/push', 'photo', {photo: 'a'}],
      [2, 'val/push', 'foo', 2],
      [1, 'val/push', 'photo', {photo: 'b'}],
    ])
  })
  it('readCallstack', () => {
    const fnBefore = jest.fn()
    const fnAfter = jest.fn()
    const log = Method('log')
    const foo = Variable('foo')
    const photo = Variable('photo')
    const logFn = (...args) => console.log(...args)
    exec(
      compile([
        foo.define(),
        foo.set(1),
        log.define(logFn),
        log.call(foo),
        photo.define(),
        photo.set({photo: 'a'}),
        foo.set(2),
        photo.set({photo: 'b'}),
        parse.readCallstack(callstack => {
          fnBefore(decallstack(callstack))
          console.log('[bytecode before walkBack]')
          console.log(callTableView(callstack))
        }),
        /* eslint-disable no-unused-vars */
        parse.walkBack({
          staticView(csdata, count) {},
          realtimeView(procedure, data, variableName, count) {},
        }),
        /* eslint-enable no-unused-vars */
        parse.readCallstack(callstack => {
          fnAfter(callTableView(callstack))
          console.log('[bytecode after walkBack]')
          console.log(callTableView(callstack))
        }),
        log.call(foo),
      ]),
    )
    expect(fnBefore.mock.calls).toEqual([
      [
        [
          {args: {name: 'photo', value: {photo: 'b'}}, procedure: 'val/push'},
          {args: {name: 'foo', value: 2}, procedure: 'val/push'},
          {args: {name: 'photo', value: {photo: 'a'}}, procedure: 'val/push'},
          {args: {name: 'photo'}, procedure: 'val/define'},
          {args: {args: ['foo'], name: 'log'}, procedure: 'fun/call'},
          {args: {name: 'log', value: logFn}, procedure: 'val/push'},
          {args: {name: 'log'}, procedure: 'val/define'},
          {args: {name: 'foo', value: 1}, procedure: 'val/push'},
          {args: {name: 'foo'}, procedure: 'val/define'},
        ],
      ],
    ])

    expect(fnAfter.mock.calls).toEqual([
      [
        [
          ['noop', {}],
          ['val/read', {fn: expect.any(Function), name: 'photo'}],
          ['val/push', {name: 'photo', value: {photo: 'b'}}],
          ['val/read', {fn: expect.any(Function), name: 'foo'}],
          ['val/push', {name: 'foo', value: 2}],
          ['val/read', {fn: expect.any(Function), name: 'photo'}],
          ['val/push', {name: 'photo', value: {photo: 'a'}}],
          ['val/read', {fn: expect.any(Function), name: 'photo'}],
          ['val/define', {name: 'photo'}],
          ['val/read', {fn: expect.any(Function), name: 'log'}],
          ['fun/call', {args: ['foo'], name: 'log'}],
          ['val/read', {fn: expect.any(Function), name: 'log'}],
          ['val/push', {name: 'log', value: logFn}],
          ['val/read', {fn: expect.any(Function), name: 'log'}],
          ['val/define', {name: 'log'}],
          ['val/read', {fn: expect.any(Function), name: 'foo'}],
          ['val/push', {name: 'foo', value: 1}],
          ['val/read', {fn: expect.any(Function), name: 'foo'}],
          ['val/define', {name: 'foo'}],
        ],
      ],
    ])
  })
})
