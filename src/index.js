//@flow

/* eslint-enable */
import {guard, assertMethodName, invariant} from './stdlib/assert'
import {word, listItem} from './stdlib/node'
import {val, fun, tag} from './coreApi'
import {compileVisitors} from './hooks/compile.hook'
import {parseVisitors} from './hooks/parse.hook'
import {runtimeVisitor} from './hooks/runtime.hook'
import {applyVisitor} from './stdlib/hook'

export function compile(
  program,
  local = {
    stack: {},
    names: [],
  },
) {
  const ctx = {}
  ctx.local = local
  ctx.cnext = parseCallstack(program)
  while (ctx.cnext && ctx.cnext.next) {
    ctx.cnow = ctx.cnext.data
    ctx.cnext = ctx.cnext.next
    ctx.args = ctx.cnow.args
    applyVisitor(ctx.cnow.procedure, ctx, compileVisitors)
  }
  return {type: 'compiled', local: ctx.local, step: ctx.step}
}
function parseCallstack(program) {
  const ctx = {}
  const callstack = listItem(null, null)
  ctx.callstack = callstack
  for (let i = 0; i < program.length; i++) {
    ctx.step = program[i]
    ctx.args = ctx.step.args
    applyVisitor(ctx.step.procedure, ctx, parseVisitors)
  }
  return ctx.callstack
}
function runtime(_) {
  runtime: while (_.execStack) {
    _.tag = _.execStack.data
    _.current = _.local.stack[_.tag]
    branch: while (_.current) {
      _.cmd = _.current.cmd
      _.arg = _.current.data
      _.next = _.current.next
      _.local.stack[_.tag] = _.current = _.next
      assertMethodName(_.cmd)

      switch (_.cmd) {
        case tag.push.tag:
          _.execStack = _.execStack.push(_.arg)
          continue runtime
        case tag.pop.tag:
          break branch
        default:
          applyVisitor(_.cmd, _, runtimeVisitor)
          continue
      }
    }
    _.execStack = _.execStack.next
  }
}

export function exec(bytecode) {
  function allocateVariables(names, localStack, val) {
    for (let i = 0, name; i < names.length; i++) {
      name = names[i]
      localStack[name] = bytecode.local.stack[name]
      val[name] = [null]
    }
  }
  let cmd, arg, next, current, scope, local, tag, execStack
  const _ = {bytecode, cmd, arg, next, current, scope, local, tag, execStack}
  _.execStack = listItem('main', null)
  _.scope = {}
  const local_ = {}
  local_.stack = {}
  local_.val = {}
  local_.stack.main = bytecode.step
  allocateVariables(bytecode.local.names, local_.stack, local_.val)
  _.local = local_
  runtime(_)
}
