//@flow

import {guard, invariant} from '../stdlib/assert'
import {val, fun, tag} from '../coreApi'
import {withScope} from '../stdlib/hook'

const defaultVisitor = {
  __defaultCase(_) {
    guard(false, 'unknown case', _.cmd, _.arg)
  },
  noop(_) {},
}

const valVisitor = {
  set(_) {
    _.local.val[_.arg.name] = [_.arg.value, _.local.val[_.arg.name]]
  },
  unset(_) {
    if (guard(_.local.val[_.arg.name].length === 2, 'pop of empty stack')) {
      _.local.val[_.arg.name] = _.local.val[_.arg.name][1]
    }
  },
  read(_) {
    if (guard(_.arg.name in _.local.val, 'no value stack')) {
      _.arg.fn(
        _.local.val[_.arg.name][0],
        _.arg.name,
        _.local.val[_.arg.name][1],
      )
    }
  },
}

const funVisitor = {
  call(_) {
    if (guard(_.arg[0] in _.local.val, 'no fun stack')) {
      const callArgs = new Array(_.arg[1].length)
      let valName
      for (let j = 0; j < _.arg[1].length; j++) {
        valName = _.arg[1][j]
        invariant(
          valName in _.local.val,
          'call of undeclared variable "%s"',
          valName,
        )
        callArgs[j] = _.local.val[valName][0]
      }
      try {
        _.local.val[_.arg[0]][0](...callArgs)
      } catch (error) {
        guard(false, 'error during execution', _.arg[0], _.arg[1])
        console.warn(error)
      }
    }
  },
}

export const runtimeVisitor = {
  ...defaultVisitor,
  ...withScope(val, valVisitor),
  ...withScope(fun, funVisitor),
}
