//@flow

import {word} from '../stdlib/node'
import {val, fun} from '../coreApi'

import {withScope} from '../stdlib/hook'

const defaultVisitor = {
  __defaultCase(ctx) {
    ctx.step = word(ctx.cnow.procedure, ctx.args, ctx.step)
  },
}

const funVisitor = {
  call(ctx) {
    const cnowArgs = ctx.cnow.args
    const args = cnowArgs.args
    const fnName = cnowArgs.name
    ctx.step = word(fun.call.tag, [fnName, args], ctx.step)
  },
}

const valVisitor = {
  define(ctx) {
    const name = ctx.args.name
    ctx.local.names.push(name)
    ctx.local.stack[name] = word(
      val.set.tag,
      [name, null],
      word('tag/pop', name, ctx.local.stack[name]),
    )
    ctx.step = word('tag/push', name, ctx.step)
  },
  set(ctx) {
    const name = ctx.args.name
    ctx.local.stack[name] = word(
      val.set.tag,
      {name, value: ctx.args.value},
      word('tag/pop', name, ctx.local.stack[name]),
    )
    ctx.step = word('tag/push', name, ctx.step)
  },
  read(ctx) {
    ctx.step = word(
      val.read.tag,
      {name: ctx.args.name, fn: ctx.args.fn},
      ctx.step,
    )
  },
}

export const compileVisitors = {
  ...defaultVisitor,
  ...withScope(fun, funVisitor),
  ...withScope(val, valVisitor),
}
