//@flow

import {richToken} from '../stdlib/node'
import {val, fun, parse} from '../coreApi'

import {withScope} from '../stdlib/hook'

const defaultVisitor = {
  __defaultCase(ctx) {
    ctx.callstack = ctx.callstack.push(ctx.step)
  },
}

const funVisitor = {
  define(ctx) {
    const definefun = ctx.callstack.push(
      richToken(val.define.tag, {name: ctx.args.name}),
    )
    ctx.callstack = definefun.push(
      richToken(val.set.tag, {name: ctx.args.name, value: ctx.args.fn}),
    )
  },
}

const parseVisitor = {
  readCallstack(ctx) {
    ctx.args.staticView(ctx.callstack, ctx)
  },
  walkBack(ctx) {
    ctx.callstack = ctx.callstack.push(richToken('noop', {}))
    const staticView = ctx.args.staticView
    const realtimeView = ctx.args.realtimeView

    let callstack = ctx.callstack
    let previousStep = null
    let count = 0
    while (callstack && callstack.next) {
      const csdata = callstack.data
      const c = count
      const added = callstack.push(
        val.read(csdata.args.name, (data, variableName) => {
          realtimeView(csdata.procedure, data, variableName, c)
        }),
      )
      if (previousStep) {
        previousStep.next = added
      }
      staticView(csdata, c)
      count += 1
      previousStep = callstack
      callstack = callstack.next
    }
  },
}

export const parseVisitors = {
  ...defaultVisitor,
  ...withScope(fun, funVisitor),
  ...withScope(parse, parseVisitor),
}
