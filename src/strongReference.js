//@flow

import {invariant, readKind} from './stdlib/assert'
import {val, fun} from './coreApi'

function StrongReference(name) {
  const state = {
    defined: false,
    updates: 0,
    get removed() {
      return this.updates < 0
    },
  }
  return {
    __kind: 'strong reference',
    name,
    state,
  }
}

function Settable(ctx) {
  return {
    set(data) {
      invariant(ctx.state.defined, 'variable "%s" is not defined', ctx.name)
      invariant(
        !ctx.state.removed,
        'variable "%s" is already removed',
        ctx.name,
      )
      ctx.state.updates += 1
      return val.set(ctx.name, data)
    },
    unset() {
      invariant(ctx.state.defined, 'variable "%s" is not defined', ctx.name)
      invariant(
        !ctx.state.removed,
        'variable "%s" is already removed',
        ctx.name,
      )
      ctx.state.updates -= 1
      return val.unset(ctx.name)
    },
  }
}

export function Variable(name) {
  const context = StrongReference(name)
  return {
    ...context,
    ...Settable(context),
    define() {
      invariant(
        !context.state.defined,
        'variable "%s" is already defined',
        context.name,
      )
      invariant(
        !context.state.removed,
        'variable "%s" is already removed',
        context.name,
      )
      context.state.defined = true
      return val.define(context.name)
    },
  }
}

export function Method(name) {
  const context = StrongReference(name)
  return {
    ...context,
    ...Settable(context),
    define(fn) {
      invariant(
        !context.state.defined,
        'method "%s" is already defined',
        context.name,
      )
      invariant(
        !context.state.removed,
        'method "%s" is already removed',
        context.name,
      )
      invariant(
        typeof fn === 'function',
        'method "%s" definition accepts only function as argument',
        context.name,
      )
      context.state.defined = true
      return fun.define(context.name, fn)
    },
    call(...args) {
      invariant(
        context.state.defined,
        'method "%s" is not defined',
        context.name,
      )
      invariant(
        !context.state.removed,
        'method "%s" is already removed',
        context.name,
      )
      const argsNames = []
      for (const arg of args) {
        if (typeof arg === 'string') {
          argsNames.push(arg)
        } else {
          invariant(
            readKind(arg) === 'strong reference',
            'unmatched argument type',
          )
          invariant(arg.state.defined, 'variable "%s" is not defined', arg.name)
          invariant(
            !arg.state.removed,
            'variable "%s" is already removed',
            arg.name,
          )
          argsNames.push(arg.name)
        }
      }
      return fun.call(context.name, ...argsNames)
    },
  }
}
