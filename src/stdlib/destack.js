//@flow

export function destack(next, val) {
  return stack => {
    let current = stack
    const result = []
    let v = val(current)
    try {
      while (current && v !== null) {
        result.push(v)
        current = next(current)
        v = val(current)
      }
    } catch (err) {}
    return result
  }
}

export const decallstack = destack(_ => _.next, _ => _.data)
export const destackdata = destack(_ => _.next, _ => [_.cmd, _.data])
