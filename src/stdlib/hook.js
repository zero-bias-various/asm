//@flow

import {guard} from './assert'

export function withScope(typedef, visitor) {
  const acc = {}
  for (const key of typedef.__names) {
    if (key in visitor) {
      acc[typedef[key].tag] = visitor[key]
    }
  }
  return acc
}

export function applyVisitor(name, ctx, acc) {
  if (name in acc) {
    acc[name](ctx)
    return
  }
  if ('__defaultCase' in acc) {
    acc.__defaultCase(ctx)
    return
  }
  guard(false, 'no case "%s" in visitor', name)
}
