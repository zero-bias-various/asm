//@flow

/* eslint-enable */
import {invariant} from './assert'
import {richToken, listItem} from './node'

function argumentsHandler(create) {
  if (Array.isArray(create))
    return (...args) => {
      const result = {}
      for (let i = 0; i < create.length && i < args.length; i++) {
        result[create[i]] = args[i]
      }
      return result
    }
  invariant(
    typeof create === 'function',
    'arguments handler should be function, got "%s"',
    typeof create,
  )
  return create
}

const scope = (rootItem => {
  let current = rootItem
  return {
    push(name) {
      if (name === null) return () => {}
      let fullName
      if (current.next) {
        fullName = `${current.data}/${name}`
      } else {
        fullName = name
      }
      current = current.push(fullName)
      return () => {
        if (current.next) current = current.next
      }
    },
    wrap(name, fn) {
      const pop = this.push(name)
      let result
      let success = false
      try {
        result = fn()
        success = true
      } finally {
        pop()
        if (!success) {
          console.warn('error during running scope "%s"', name)
        }
      }
      return result
    },
    read: () => current.data,
  }
})(listItem(null, null))

function fork() {
  return scope.wrap(this.tag, () => {
    const forked = (...args) => richToken(forked.tag, forked.create(...args))
    forked.__kind = 'fork'
    forked.tag = scope.read()
    forked.create = this.create
    forked.fork = this.fork
    return forked
  })
}
export function def(definition) {
  const result = {}
  result.__kind = 'definition'
  result.tag = definition.name
  result.create = argumentsHandler(definition.create)
  result.fork = fork
  return result
}

export function scopeDef(name, resolver) {
  return scope.wrap(name, () => {
    const result = {}
    result.__names = []
    for (const [key, definition] of Object.entries(resolver)) {
      result[key] = def(definition).fork()
      result.__names.push(key)
    }
    return result
  })
}
