//@flow

function printf(message: string, args: any[]) {
  let index = 0
  return message.replace(/%s/g, _match => String(args[index++]))
}

export function invariant(cond: any, message: string, ...args: any[]) {
  if (cond) return
  throw new Error(printf(message, args))
}

export function guard(cond, message, ...args) {
  if (cond) return true
  console.warn(message, ...args)
  return false
}
export function isInternalField(name) {
  switch (name) {
    case '__names':
    case '__kind':
    case '__defaultCase':
      return true
    default:
      return false
  }
}
export function readKind(value: mixed) {
  const isObject = typeof value === 'object' && value !== null
  if (!isObject) return null
  const kindExists = '__kind' in value
  if (!kindExists) return null
  return value.__kind
}
export function assertMethodName(name) {
  switch (name) {
    case 'tag/push':
    case 'tag/pop':
    case 'val/define':
    case 'val/push':
    case 'val/pop':
    case 'val/read':
    case 'fun/define':
    case 'fun/call':
    case 'noop':
      break
    default:
      throw new Error(printf('unknown command "%s"', [name]))
  }
}
