//@flow

export function richToken(procedure, namedArguments) {
  return {
    procedure,
    args: namedArguments,
  }
}
export function word(cmd, data, next) {
  return {cmd, data, next}
}
let nextID = 0
class ListItem {
  id = ++nextID
  data: any
  next: ListItem | null
  constructor(data: any, next: ListItem | null) {
    this.data = data
    this.next = next
  }
  push(data) {
    return new ListItem(data, this)
  }
}

export function listItem(data: any, next: ListItem | null) {
  return new ListItem(data, next)
}
