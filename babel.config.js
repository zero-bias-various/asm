//@flow

module.exports = api => {
  api.cache.never()
  const plugins = [
    '@babel/plugin-proposal-class-properties'
  ]
  if (process.env.NODE_ENV === 'test') {
    plugins.push('@babel/plugin-transform-modules-commonjs')
  }
  return {
    presets: [
      [
        '@babel/preset-env',
        {
          shippedProposals: true,
          targets: {
            node: 'current',
          },
        },
      ],
      '@babel/preset-flow',
    ],
    plugins,
  }
}
