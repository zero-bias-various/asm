//@flow

module.exports = {
  automock: false,
  displayName: 'asm.engine',
  // preset: 'jest-puppeteer',
  browser: false,
  testEnvironment: 'node',

  // globalSetup: './setup.js',
  // globalTeardown: './teardown.js',
  // testEnvironment: './headless_environment.js',
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  // setupTestFrameworkScriptFile: `${__dirname}/__tests__/setup/setupTests`,
  notify: false,
  // notifyMode: 'change',
  testMatch: [
    // '<rootDir>/*.test.js'
    '<rootDir>/src/**/*.spec.js',
  ],
  globals: {
    __DEV__: true,
  },
  moduleFileExtensions: ['js', 'jsx'],
  // watchPathIgnorePatterns: ['.*jest-stare.*\\.js'],
  // reporters: ['default', 'jest-stare'],
  // modulePathIgnorePatterns: ['<rootDir>/src/model-storage'],
}
